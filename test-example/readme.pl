use strict;
use warnings;

my $file_name = $0;
print "my file: $file_name\n";

open( my $fh, '<', $file_name ) or die "Can't open $file_name: $!";

my $count = 0;
while ( my $line = <$fh> ) {
    $count += 1;
    print "Me, line $count: $line";
}

close $fh;
