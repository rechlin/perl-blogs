#!/usr/bin/perl
use warnings;
use diagnostics;
use strict;

# test a module

use Test::More;

use FindBin qw($Bin);
use lib "$Bin/.";

use readFile qw(readFile);

my $file_name = "stumble.txt";
is( readFile( $file_name, '' ), '1', 'Read file: ' . $file_name);

done_testing();
