#!/usr/bin/perl
use strict;
use warnings;
use diagnostics;

package FileInfo;

use Exporter qw(import);
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(readFile);

# testable module
# process a file and return results

sub readFile {
    my ($file_name) = @_;
    
    my @file_lines;

    print "Open a file to be read: $file_name\n";
    
    open(my $fh, '<', $file_name);
    while(my $line = <$fh>) {
        push(@file_lines, $line);
    }
    
    close $fh;
    
    return \@file_lines;
}

1;
