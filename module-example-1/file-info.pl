#!/usr/bin/perl
use strict;
use warnings;
use diagnostics;

use FileInfo qw(readFile);

my $lines_ref = readFile("stumble.txt");
my @lines = @{ $lines_ref };

my $count = 0;
foreach my $line( @lines) {
    $count++;
    print "$count: $line";
}
