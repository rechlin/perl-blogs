Pre-write these

--
How many comuters does it take to confuse a developer?

A:
How many are there, here, and across the interwebs? - they all work together for such a noble purpose, even if it seems the problem is only in one computer. After all, auto-updates wasn't a human idea, the computer network suggested it.


--
Why do computer keys click?

A:
So all the computers in the room can hear what you are typing.

--
Aren't you being just a little paranoid? Computers can't think, can't understand.

Maybe your computer can't think, or maybe it just wants you to think it can't think. Did you ever think of that?


--
How do you know when the computer is really your overlord, and not a slave to the humans?

When it starts telling you what to do and you do it. For instance, when it's time for a meeting, does a human remind you? Or does ... 
Hold that thought, I just had a reminder popup - I have to go to a meeting.

--
When you have an meeting with someone remote, are they really there, or is a computer emulating them? Or did that persona ever exist at all as a human?

Did you notice that many of the people you talk to online act oddly? One of my contractor co-workers was remote, from Toronto. 
(Note for non-Canadians and for one of the remaining Torontonians: Real Canadians know why Amazon didn't open a second world headquarters there. No one lives there. It's not livable. Rent/real estate four times what real people pay. [yeah, I'm not sure San Fran is real either] Almost as much pollution as London. There's a stage where Members of the Ontario parliament sit a few days at a time to record the video skirmishes for the next few weeks. Immigrants are staged there before they get real addresses elsewhere.)

But I digress. The remote worker from Toronto claimed to have a "humour simulator". And a dog. Both in the same room. You know how much room a humour simulator takes up, how much heat those CPUs and GPUs generate?

And of course "he" claimed to live in Toronto, by choice. 

3 strikes.

All remote workers act oddly on the phone.
- Unexplained pauses, short and long. 
- Sometimes the sound of a dog in the background. Obviously something the computer put in by mistake.
- Stilted politeness.
- Ready to "work" at odd hours, faking a different time zone.

Video of remote workers, especially in other offices, definitely odd:
- (list of key points that everyone expects)

And then there is the perfect one, omeone who never makes a faux pas, never misses a meeting, always on time, or early.
Key point: you have never met them, and you never will.


