Notes

Notes on the design of the series of Perl articles

Follow outline in my first article

- Functions
- Blocks
- Loops and Decisions
- Comments: line and block
- Variables
- Passing variables to a function: by value, by reference
- Using the public package repository, and package manager
- Breaking long lines into readable pieces that fit in a fairly narrow window, so you can edit 3 files side-by-side

Include humour!

Humor-only, or general software topics daily?

topics for humor, pre-write if needed?
