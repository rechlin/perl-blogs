This is likely to turn into the README.md

Blog post for starting with Perl as a 2nd language

# Perl as a second Language: Step 1

## Write a file that does something

```
use strict;
use warnings;

print "hello world 1\n";
print("hello world, with brackets\n");
```

Line 1: use strict;
Line 2: use warnings;
Always start your Perl files with these two lines.
The first allows Perl to report errors caused by syntax errors and spelling errors.
The second enables all warnings in the block it is in, after the point where it occurs.
This means that putting it at the top of a file enables all warnings for all the code in the file.
The docs will tell you how to disable some/all warnings in some code if needed.

Line 4: print, without brackets
Line 5: print, using brackets

You can call functions with or without brackets.

This is an example of Perl's philosphy, "There is more than one way to do it", spellese "TIMTOWTDI", also spelled "TMTOWTDI", and pronounced "Tim Toady".

There is another philosophy, of course, that 



# Perl as a second language: Step 2

subtitle: What is Perl code like?

Perl Philosophies
- yes, it's a plural. 
- `https://en.wikipedia.org/wiki/There%27s_more_than_one_way_to_do_it`
- Do I have to write Perl code that looks like line noise?
  - No: http://www.perlmonks.org/?node_id=463928
- TIMTOWTDIBSCINABTE (pronounced – “Tim Toady Bicarbonate”) There Is More Than One Way To Do It, But Sometimes Consistency Is Not A Bad Thing Either
  - Enlightened Perl Organization - https://ww2.enlightenedperl.org/

Things you may need to consider:
- UTF-8: http://www.perlmonks.org/?node_id=1211014
- "Got the data, now what?" - http://www.perlmonks.org/?node_id=928819
- "A Design Philosophy" - http://www.perlmonks.org/?node_id=792755


